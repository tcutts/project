const _data = {
    "instruments": [
        {
            "id": 0,
            "category": "Berimbau",
            "desc": "Stringed instrument to be played for capoeira"
        },
        {
            "id": 1,
            "category": "Atabaque",
            "desc": "Drum for capoeira"
        },
        {
            "id": 2,
            "category": "Pandeiro",
            "desc": "Hand drum for capoeira"
        },
    ]
}

if (!sessionStorage.getItem('loaded')){
    sessionStorage.setItem('instrumentData', JSON.stringify(_data))
    sessionStorage.setItem('loaded', true)
}

export function mock_getinstruments(){
    const strData = sessionStorage.getItem('instrumentData');
    const objData = JSON.parse(strData);
    return objData;
}

export function mock_createInstruments(category, desc){
    const strData = sessionStorage.getItem('instrumentData');
    const objData = JSON.parse(strData);
    
    const arrInstruments = objData['instruments'];

    const id = arrInstruments.length;

    arrInstruments[arrInstruments.length] = {id,category,desc}

    sessionStorage.setItem('instrumentData', JSON.stringify(objData))
}

export function mock_deleteInstrument(id){
    const strData = sessionStorage.getItem('instrumentData');
    const objData = JSON.parse(strData);
    
    const arrInstruments = objData['instruments'];

    
    const index = arrInstruments.map(function(e) { return e.id; }).indexOf(id);
    if (index > -1) {
        arrInstruments.splice(index, 1);
    }



    sessionStorage.setItem('instrumentData', JSON.stringify(objData))
}


export function mock_getInstrument(id){
    const strData = sessionStorage.getItem('instrumentData');
    const objData = JSON.parse(strData);
    
    const arrInstruments = objData['instruments'];

    
    const index = arrInstruments.map(function(e) { return e.id; }).indexOf(id);
    if (index > -1) {
        
        return arrInstruments[index]
    }
}

export function mock_updateInstrument(id, newCatergoryVal, newDescriptionVal){
    const strData = sessionStorage.getItem('instrumentData');
    const objData = JSON.parse(strData);
    
    const arrInstruments = objData['instruments'];

    
    const index = arrInstruments.map(function(e) { return e.id; }).indexOf(id);
    if (index > -1) {
        arrInstruments[index].category = newCatergoryVal
        arrInstruments[index].desc = newDescriptionVal
        
        sessionStorage.setItem('instrumentData', JSON.stringify(objData))
    }
}