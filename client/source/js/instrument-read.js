import {mock_getinstruments} from '../../test/mock-data.js'
import {deleteInstrument} from './instrument-delete.js'
import {updateInstrument} from './instrument-update.js'

window.deleteInstrument = deleteInstrument
window.updateInstrument = updateInstrument

function getCard(id, catagory, desc) {
    return `
    <div class="col-4 py-3">
        <div id= "${id}" class="card" style="width: 18rem;">
            <div class ="card-header text-end">
            <button type="button" class="btn-close" onclick="deleteInstrument(${id})"></button>
            </div>
            <div class="card-body">

                <h5 class="card-title">${catagory}</h5>
                <p class="card-text">${desc}</p>
                <input class="btn btn-primary" type="button" value="edit" onclick="updateInstrument(${id})">
            </div>
         </div>
    </div>`
}

export function readinstruments() {

    const rowHtml = `<div id="instrumentrow" class="row justify-content-center-start"></div>`
    const domReadinstrumentsContainer = document.getElementById("instrumentRead")
    domReadinstrumentsContainer.innerHTML = rowHtml;

    const data = mock_getinstruments()
    const instruments = data["instruments"]

    for(let i=0; i<instruments.length; i++){
        const cardHtml = getCard(instruments[i].id,instruments[i].category,instruments[i].desc) 
        const rowDom =  document.getElementById("instrumentrow")
        rowDom.innerHTML += cardHtml
    }

   
}