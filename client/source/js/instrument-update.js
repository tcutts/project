import {mock_getInstrument, mock_updateInstrument} from '../../test/mock-data.js'

function updateModal(desc, category) {
    return `
    <div class="modal fade" id="updateInstrumentModal">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Update Instrument</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
        <form>
            <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Instrument Type:</label>
            <input type="text" class="form-control" id="instrumentCatagory" value = "${category}">
            </div>
            <div class="mb-3">
            <label for="message-text" class="col-form-label">Description:</label>
            <textarea class="form-control" id="instrumentDescription">${desc}</textarea>
            </div>
        </form>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Update</button>
        </div>
    </div>
    </div>
    </div>
    `
}





export function updateInstrument(id) {
    
    const instruments = mock_getInstrument(id)
    

    document.body.insertAdjacentHTML('afterbegin', updateModal(instruments.desc, instruments.category))
    var myModalEl = document.getElementById('updateInstrumentModal')
    myModalEl.addEventListener('hidden.bs.modal', (event) => {

        const newCatergoryVal = document.getElementById("instrumentCatagory").value
        const newDescriptionVal = document.getElementById("instrumentDescription").value

        if (newCatergoryVal !== instruments.category || newDescriptionVal !== instruments.desc){
            mock_updateInstrument(id,newCatergoryVal,newDescriptionVal)
            location.reload()
        }

    })

    var myModal = new bootstrap.Modal(document.getElementById('updateInstrumentModal'), {})
    myModal.show()
}