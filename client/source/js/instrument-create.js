import {mock_createInstruments} from '../../test/mock-data.js'

export function onCreate() {
    const elCatagory = document.getElementById("instrumentCatagory");
    const elDesc= document.getElementById("instrumentDescription");

    mock_createInstruments(elCatagory.value, elDesc.value);

    elCatagory.value = null;
    elDesc.value = null;
}