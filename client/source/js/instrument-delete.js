import {mock_deleteInstrument} from '../../test/mock-data.js'

export function deleteInstrument(id) {
    mock_deleteInstrument(id)
    location.reload()
}