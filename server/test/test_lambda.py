import pytest
import json


from test.setup.table_setup import create_table, delete_table
from app.instrument_create import lambda_handler as dt_create_lambda_handler

@pytest.fixture
def create_event():
    with open(".\\events\\create_event.json") as json_file:
        return json.load(json_file)

def test_lambda_funcs():
    event = create_event()
    assert dt_create_lambda_handler(event, None)  == None